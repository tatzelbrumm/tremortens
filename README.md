# Linderung von Tremor mit TENS-Gerät  

*Dr. sc. techn. Christoph Maier*

## Problemstellung
Im FabLab hat mich jemand gebeten, ihm beim Löten zu helfen.  
Das habe ich bei den ersten Lötverbindungen getan und ihn dann gebeten,  
den Rest selbst zu löten.  

Er hat mir gesagt, das könne er nicht, weil seine Hand zu sehr zittere.  

## Lösungsansatz
Daraufhin hatte ich die Idee, ein [TENS-Gerät](http://www.tenspainrelief.com/lg-3000-review/), das ich aus den USA mitgebracht habe, zu verwenden, um das Zittern zu verringern.  

Ohne Reizstromstimulation existiert ein unwillkürliches Zittern (Tremor), 
und bei starker Reizstromstimulation erzeugt der externe Strom unwillkürliche Bewegungen.

Zwischen keiner Reizstromstimulation und zu starker Reizstromstimulation sollte ein 
Optimum an Stimulation zu finden sein, in dem der Tremor minimal ist.  

## Bewertung der Wirksamkeit
Der Erfolg einer Linderung von Tremor mit einem TENS-Gerät lässt sich 
unmittelbar damit messen, dass Tätigkeiten, die Feinmotorik erfordern,
ermöglicht oder zumindest erleichtert werden. 

### Keine medizinische Behandlung im herkömmlichen Sinn
Im Gegensatz zu einem klinischen Test muss der Erfolg des Verfahrens 
nicht auf andere übertragbar und hinreichend statistisch belegt sein.  

Es geht hier ganz konkret darum, einer einzelnen Person das Löten zu ermöglichen, 
in dem ich derjenigen ein Hilfsmittel in die Hand gebe, das sie selbst bedienen und auf ihre Bedürfnisse hin optimieren kann. Die Trennung zwischen Arzt und Patient existiert nicht - ich hätte auch gar nicht die medizinische Qualifikation, als Arzt aufzutreten.

### Messbares Erfolgskriterium
Trotzdem ist es sinnvoll, ein messbares Kriterium für den Erfolg des Verfahrens 
zu definieren, schon um eine optimale Plazierung der Elektroden und der
Reizstromeinstellungen zu finden. 
Für ein messbares Erfolgskriterium eignet sich zum Beispiel die
[SMD Challenge](https://www.tindie.com/products/MakersBox/smd-challenge/)-Platine 
oder ähnliche, noch zu entwickelnde Platinen, 
mit denen sich feinmotorische Fähigkeiten quantitativ messen lassen.  

Alternativ dazu ist es auch denkbar, die Reduktion des Tremors durch Handschriftproben zu erfassen:  
die Person schreibt 100 mal  

> "Ich möchte meinen Tremor los werden!"

und variiert dabei systematisch die Position der Elektroden und die Einstellungen des Reizstroms.

## Materialien und Methoden

### TENS
Transkutane elektrische Neuro-Stimulatoren (TENS) sind kostengünstige Geräte, 
die geeignet sind, von Menschen ohne spezifische medizinische Vorkenntnisse ohne Gesundheitsgefährdung benutzt zu werden. 

Auch in Deutschland sind Geräte, z.B. von [PEARL](https://www.pearl.de/a-NC5023-4010.shtml), günstig erhältlich.

### Methode in der Literatur
Auch wenn mir [Christoph Maier] die Methode spontan eingefallen ist, 
als jemand mit Tremor mir gesagt hatte, dass er deshalb nicht löten könne,
bin ich weder der erste noch der einzige, dem dazu eingefallen ist,
den Tremor mit einem TENS-Gerät zu lindern.  

Die Autoren

* Onanong Jitkritsadakul,
* Chusak Thanawattano, 
* Chanawat Anan und 
* Roongroj Bhidayasiria

aus Thailand haben zu diesem Thema Geräte entwickelt, eine klinische Studie durchgeführt und die Ergebnisse publiziert:


  1. Jitkritsadakul O, Thanawattano C, Anan C, Bhidayasiri R. 
[Tremor's glove-an innovative electrical muscle stimulation therapy for intractable tremor in Parkinson's disease: A randomized sham-controlled trial.](https://www.ncbi.nlm.nih.gov/pubmed/28991711) 
J Neurol Sci. 2017 Oct 15;381:331-340. doi: 10.1016/j.jns.2017.08.3246. Epub 2017 Aug 24.  
  1. O. Jitkritsadakul, C. Thanawattano, C. Anan, R. Bhidayasiri [Tremor’s Glove – An Innovative Electrical Muscle Stimulation Therapy to Treat Intractable Tremor in Parkinson’s Disease Patients: A Randomized Sham-Controlled Trial](http://www.mdsabstracts.org/abstract/tremors-glove-an-innovative-electrical-muscle-stimulation-therapy-to-treat-intractable-tremor-in-parkinsons-disease-patients-a-randomized-sham-controlled-trial/) 21st International Congress of the International Parkinson and Movement Disorder Society; Parkinson’s Disease: Clinical Trials, Pharmacology And Treatment, Vancouver, BC, June 8, 2017  
  1. Onanong Jitkritsadakul, Chusak Thanawattano, Chanawat Anan, Roongroj Bhidayasiria [Exploring the effect of electrical muscle stimulation as a novel treatment of intractable tremor in Parkinson's disease](https://www.sciencedirect.com/science/article/pii/S0022510X15020237) Journal of the Neurological Sciences
Volume 358, Issues 1–2, 15 November 2015, Pages 146-152  
  Jitkritsadakul O, Thanawattano C, Anan C, Bhidayasiri R. 
[Exploring the effect of electrical muscle stimulation as a novel treatment of intractable tremor in Parkinson's disease.](https://www.ncbi.nlm.nih.gov/pubmed/26342942)
J Neurol Sci. 2015 Nov 15;358(1-2):146-52. doi: 10.1016/j.jns.2015.08.1527. Epub 2015 Aug 28.  

## Risiken und Nebenwirkungen
> ICH BIN WEDER ARZT NOCH APOTHEKER!!!

### Risikoabschätzung
Die zur Anwendung kommenden TENS-Geräte sind für die Verwendung ohne ärztliche Aufsicht konzipiert
und somit —außer bei grob unsachgemäßer Verwendung— medizinisch unbedenklich. 

Die Unbedenklichkeit wird durch den von Jitkrisidakul et al. durchgeführten klinischen Test unter
ärztlicher Aufsicht bestätigt. 

### Nebenwirkungen
Die Person, die mich um Hilfe beim Löten gebeten hat, 
hat das im Rahmen eines Projekts bei **ttg-team training** getan, 
das letztlich zur Wiedereingliderung in den Arbeitsmarkt dient.  

Durch das von dieser Person in eigener Verantwortung durchgeführte Verfahren 
zur Linderung ihres Tremors wird diese gleichzeitig wieder an wissenschaftliches Arbeiten 
herangeführt. Einem Physik-Vordiplom entsprechende Vorkenntnisse können vorausgesetzt werden. 

Dadurch ist zu erwarten, dass mit der Linderung des Tremorsymptoms gleichzeitig auch dessen 
Ursache bekämpft wird, so dass letztlich die erfolgreiche Erfahrung von Selbsthilfe 
die Situation dieser Person positiver beeinflusst als die Symptombehandlung.
